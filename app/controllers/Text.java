package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.plot.Plot;
import controllers.plot.PlotWeek;
import models.Activity;
import models.TI;
import models.User;
import play.libs.Json;

import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.ListIterator;

public class Text {

    // Language
    @SuppressWarnings("unused")
    private static final int ENGLISH = 0;

    private static final String brand = "Where goes my Time?";
    private static final boolean userbtnDivided = false;
    private static final String userbtnLinks[][] = {
            {" Settings", "#settings", "gear"},
            {" Log Out", "logout()", "power-off"}
    };
    private static final String navbarSectionsDash[][] = {
            {" Home", "initial", "desktop"},
            {" Semana Atual", "week", "bar-chart-o"},
            {" Histórico", "history", "table"},
            {" Registrar", "demo", "plus"},
            {" Tempo Investido", "initial.registerTI", ""},
            {" Atividade", "initial.registerActivity", ""}
    };
    private static final String navbarSectionsHome[][] = {
            {" Sobre", "#about"},
            {" Serviços", "h#services"},
            {" Contato", "#contact"}
    };
    private static final String footbarSectionsHome[][] = {
            {" Home", "#home"},
            {" Sobre", "#about"},
            {" Serviços", "#services"},
            {" Contato", "#contact"}
    };
    private static final String [] homepageHeader1 = {"Coloque suas tarefas aqui!"};
    private static final String [] homepageHeader2 = {"Intuitivo"};
    private static final String [] homepageHeader3 = {"Sobre nós,", "a equipe"};
    private static final String homepageContent1 = "<span>Acompanhe o tempo que você gasta em cada um de suas atividades." +
            " Registre o tempo investido nas atividades ao longo do tempo, e nosso gráficos mostrarão com detalhes, qual" +
            " atividade está custando mais do seu tempo. Utilize tags para visualizar melhor os gráficos, e compare semanas" +
            " com outras.</span>";
    private static final String homepageContent2 = "Utilize WGMT do computador, do celular, ou via tablet. Basta utilizar " +
            "o seu browser de preferência. Seus dados vão sendo salvos em sua conta, sendo recuperados onde quer que você " +
            "acesse.";
    private static final String homepageContent3 = "Compomos uma equipe feita para uma disciplina de Engenharia de Software" +
            " na UFCG. Somos <a target=\"_blank\" href=\"https://github.com/ddspog\">" +
            "Dennis</a>, <a target=\"_blank\" href=\"https://github.com/gabriellemos\">" +
            "Gabriel</a>, <a target=\"_blank\" href=\"https://github.com/jordangba\">" +
            "Jordan</a>, <a target=\"_blank\" href=\"https://github.com/andrewfnw\">" +
            "Andrew</a> e <a target=\"_blank\" href=\"https://github.com/alineatcosta\">Aline</a>.";
    private static final String footbarText = "Copyright © FriendlyFood 2016. Todos os direitos reservados.";
    private static final String timeInvested = "Tempo Investido";
    private static final String viewAllTis = "Ver todas as TIs  ";
    private static final String initialscreen = "Tela Inicial";
    private static final String weekscreen = "Semana Atual";
    private static final String historyscreen = "Histórico";

    private static final String recentactivity = "Atividades Recentes";
    private static final String viewAllActivities = "Ver todas as Atividades  ";
    private static final String modalActivityTitle = "Cadastro de Nova Atividade";
    private static final String modalTITitle = "Cadastro de Tempo Investido";
    private static final String modalPlaceholderActivity = "Nome da Atividade";
    private static final String modalPlaceholderPriority = "Prioridade da Atividade";
    private static final String modalPlaceholderTimeTI = "Tempo gasto na Atividade (em horas)";
    private static final String close = "Fechar";
    private static final String submit = "Salvar";

    private static final String registerActivityText = "\tColoque aqui suas atividades de lazer ou trabalho," +
                                                    "e tenha um acompanhamento melhor de como você divide seu tempo.";
    private static final String registerActivityBtn = "Cadastrar Atividade";
    private static final String registerTIText = "\tPara cada atividade, você pode adicionar quanto Tempo Investido (Ti) você gastou na atividade escolhida.";
    private static final String registerTIBtn = "Cadastrar Ti";

    private static final String weekGraphName = " Atividades da Semana Atual";
    private static final String weekRankingName = " Ranking de Atividades";
    private static final String weekRankingPlaceName = "Posição";
    private static final String weekRankingActivityName = "Atividade";
    private static final String weekRankingTotalName = "Total de Horas";
    private static final String weekRankingProportionName = "Percentagem";
    private static final String historyGraphName = " Atividades do período:";
    private static final String historyCompare = "Compare";
    private static final String historyWith = "com";
    private static final String weekOption1 = "Semana atual";
    private static final String weekOption2 = "Semana passada";
    private static final String weekOption3 = "Há duas semanas";

    private static final String imageFolder = "/assets/images/";
    private static final String homepageContentImg1 = "ipad.png";
    private static final String homepageContentImg2 = "dog.png";
    private static final String homepageContentImg3 = "phones.png";
    private static final String homepageHeaderTextMainTitle = "Where goes my Time?";
    private static final String homepageHeaderTextSubTitle = "Gerencie seu tempo";
    private static final String homepageBannerMessage = "Conecte-se agora!";
    private static final String [] socialBtnGoogle = {"/login", "Login via Google", "google"};

    public static JsonNode getNavbarText(String username) {
        ObjectNode item = Json.newObject();

        item.put("brand", brand);
        item.put("username", username);

        ObjectNode options = Json.newObject();
        ArrayNode before = new ArrayNode(JsonNodeFactory.instance);
        ArrayNode after = new ArrayNode(JsonNodeFactory.instance);

        after.add(getUserBtnLink(1));

        options.put("divided", "" + userbtnDivided);
        options.set("before", before);
        options.set("after", after);

        item.set("userbtn", options);

        ArrayNode navbar = new ArrayNode(JsonNodeFactory.instance);
        navbar.add(getNavbarDashSecLink(0, false, new int[]{}));
        navbar.add(getNavbarDashSecLink(1, false, new int[]{}));
        navbar.add(getNavbarDashSecLink(2, false, new int[]{}));
        navbar.add(getNavbarDashSecLink(3, true, new int[]{4, 5}));

        item.set("navbarbtn", navbar);

        return item;
    }

    public static JsonNode getNavbarText() {
        ObjectNode item = Json.newObject();

        item.put("brand", brand);

        ArrayNode navbar = new ArrayNode(JsonNodeFactory.instance);
        navbar.add(getNavbarHomeSecLink(0));
        navbar.add(getNavbarHomeSecLink(1));
        navbar.add(getNavbarHomeSecLink(2));
        item.set("navbarbtn", navbar);

        return item;
    }

    public static JsonNode getFootbarText(){
        ObjectNode item = Json.newObject();

        ArrayNode footbar = new ArrayNode(JsonNodeFactory.instance);
        footbar.add(getFootbarHomeSecLink(0));
        footbar.add(getFootbarHomeSecLink(1));
        footbar.add(getFootbarHomeSecLink(2));
        footbar.add(getFootbarHomeSecLink(3));
        item.set("footbarbtn", footbar);

        item.put("foottext", footbarText);

        return item;
    }

    private static JsonNode getNavbarHomeSecLink(int index) {
        ObjectNode item = Json.newObject();
        item.put("text", navbarSectionsHome[index][0]);
        item.put("url", navbarSectionsHome[index][1]);
        return item;
    }

    private static JsonNode getFootbarHomeSecLink(int index) {
        ObjectNode item = Json.newObject();
        item.put("text", footbarSectionsHome[index][0]);
        item.put("url", footbarSectionsHome[index][1]);
        return item;

    }

    private static JsonNode getNavbarDashSecLink(int index, boolean hasChild, int[] children){
        ObjectNode item = Json.newObject();
        item.put("text", navbarSectionsDash[index][0]);
        item.put("url", navbarSectionsDash[index][1]);
        item.put("icon", navbarSectionsDash[index][2]);
        item.put("hasChild", hasChild);

        ArrayNode childs = new ArrayNode(JsonNodeFactory.instance);
        if(hasChild){
            for (int i : children) {
                childs.add(getNavbarDashSecLink(i, false, new int[]{}));
            }
        }
        item.set("children", childs);

        return item;
    }

    private static JsonNode getUserBtnLink(int index){
        ObjectNode item = Json.newObject();
        item.put("text", userbtnLinks[index][0]);
        item.put("url",  userbtnLinks[index][1]);
        item.put("icon",  userbtnLinks[index][2]);
        return item;
    }

    public static JsonNode getLastTisText(User us) {
        ArrayNode list = new ArrayNode(JsonNodeFactory.instance);

        int counter = 8;
        ListIterator<TI> it = us.getTis().listIterator(us.getTis().size());
        while(it.hasPrevious() && counter-- > 0){
            TI t = it.previous();
            ObjectNode item = Json.newObject();
            item.put("text", " " + t.getActivity().getName() + " - " + t.getTime() + " horas");
            item.put("date", t.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
            list.add(item);
        }

        ObjectNode result = Json.newObject();
        result.set("list", list);
        result.put("timeinvested", timeInvested);
        result.put("viewall", viewAllTis);

        return result;
    }

    public static JsonNode getLastActivitesText() {
        ObjectNode result = Json.newObject();
        result.put("recentactivity", recentactivity);
        result.put("viewall", viewAllActivities);

        return result;
    }

    public static JsonNode getLastActivities(User us, int page) {
        ArrayNode result = new ArrayNode(JsonNodeFactory.instance);

        Iterator<Activity> it = us.getActivities().iterator();

        int counter = 8 * (page - 1);
        while (it.hasNext() && counter-- > 0)
            it.next();
        counter = 8;
        while(it.hasNext() && counter-- > 0){
            Activity a = it.next();
            ObjectNode item = Json.newObject();
            item.put("text", a.getName());
            item.put("id", a.getId());
            result.add(item);
        }

        return result;
    }

    public static JsonNode getGraphData(Plot plot) {
        ObjectNode result = Json.newObject();

        result.set("period", plot.getPeriod());
        result.set("barsData", plot.getBars());
        result.set("barsLabels", plot.getLabels());
        result.set("barsY", plot.getY());
        result.set("barsColors", plot.getColors());
        return result;
    }


    public static JsonNode getLabelInitialPage(){
        ObjectNode result = Json.newObject();
        result.put("initialscreen", initialscreen);
        result.put("registerActivityText", registerActivityText);
        result.put("registerActivityBtn", registerActivityBtn);
        result.put("registerTIText", registerTIText);
        result.put("registerTIBtn", registerTIBtn);
        return result;
    }

    public static JsonNode getLabelWeekPage() {
        ObjectNode result = Json.newObject();
        result.put("weekscreen", weekscreen);
        result.put("graphName", weekGraphName);
        result.put("rankingName", weekRankingName);
        result.put("rankingPlaceName", weekRankingPlaceName);
        result.put("rankingActivityName", weekRankingActivityName);
        result.put("rankingTotalName", weekRankingTotalName);
        result.put("rankingProportionName", weekRankingProportionName);
        return result;
    }

    public static JsonNode getLabelHistoryPage() {
        ObjectNode result = Json.newObject();
        result.put("historyscreen", historyscreen);
        result.put("graphName", historyGraphName);
        result.put("compare", historyCompare);
        result.put("with", historyWith);

        ArrayNode options = new ArrayNode(JsonNodeFactory.instance);
        ObjectNode opt1 = Json.newObject();
        opt1.put("text", weekOption1);
        opt1.put("value", 0);
        options.add(opt1);
        ObjectNode opt2 = Json.newObject();
        opt2.put("text", weekOption2);
        opt2.put("value", 1);
        options.add(opt2);
        ObjectNode opt3 = Json.newObject();
        opt3.put("text", weekOption3);
        opt3.put("value", 2);
        options.add(opt3);

        result.set("options", options);

        return result;
    }

    public static JsonNode getModalTiText(User user) {
        ObjectNode result = Json.newObject();
        result.put("title", modalTITitle);
        result.put("placeholderActivity", modalPlaceholderActivity);
        result.put("placeholderTimeTI", modalPlaceholderTimeTI);

        ArrayNode activities = new ArrayNode(JsonNodeFactory.instance);
        user.getActivities().forEach(a -> {
            ObjectNode item = Json.newObject();
            item.put("name", a.getName());
            activities.add(item);
        });
        result.set("activities", activities);

        return result;
    }

    public static JsonNode getModalActivityText(User user) {
        ObjectNode result = Json.newObject();
        result.put("title", modalActivityTitle);
        result.put("placeholderActivity", modalPlaceholderActivity);
        result.put("placeholderPriority", modalPlaceholderPriority);

        ArrayNode options = new ArrayNode(JsonNodeFactory.instance);
        ObjectNode low = Json.newObject();
        low.put("text", "Baixa");
        low.put("id", "" + -1);
        options.add(low);

        ObjectNode medium = Json.newObject();
        medium.put("text", "Média");
        medium.put("id", "" + 0);
        options.add(medium);

        ObjectNode high = Json.newObject();
        high.put("text", "Alta");
        high.put("id", "" + 1);
        options.add(high);

        result.set("priorities", options);

        ArrayNode activities = new ArrayNode(JsonNodeFactory.instance);
        user.getActivities().forEach(a -> {
                ObjectNode item = Json.newObject();
                item.put("name", a.getName());
                activities.add(item);
        });
        result.set("activities", activities);

        return result;
    }

    public static JsonNode getRankingData(PlotWeek plot) {
        ObjectNode result = Json.newObject();
        result.set("list", plot.getRanking());

        return result;
    }

    public static JsonNode getHistoryOptions(Object user) {
        User us = (User) user;
        ObjectNode result = Json.newObject();

        result.put("weekA", "" + us.getPreferences().getPeriodSelectedInHistoryA().id);
        result.put("weekB", "" + us.getPreferences().getPeriodSelectedInHistoryB().id);

        return result;
    }

    public static JsonNode getContentHomepage() {
        ArrayNode content = new ArrayNode(JsonNodeFactory.instance);

        ObjectNode ct1 = Json.newObject();
        ArrayNode hd1 = new ArrayNode(JsonNodeFactory.instance);
        for (String s : homepageHeader1)
            hd1.add(s);
        ct1.set("header", hd1);
        ct1.put("content", homepageContent1);
        ct1.put("imglink", imageFolder + homepageContentImg1);

        ObjectNode ct2 = Json.newObject();
        ArrayNode hd2 = new ArrayNode(JsonNodeFactory.instance);
        for (String s : homepageHeader2)
            hd2.add(s);
        ct2.set("header", hd2);
        ct2.put("content", homepageContent2);
        ct2.put("imglink", imageFolder + homepageContentImg2);

        ObjectNode ct3 = Json.newObject();
        ArrayNode hd3 = new ArrayNode(JsonNodeFactory.instance);
        for (String s : homepageHeader3)
            hd3.add(s);
        ct3.set("header", hd3);
        ct3.put("content", homepageContent3);
        ct3.put("imglink", imageFolder + homepageContentImg3);

        content.add(ct1);
        content.add(ct2);
        content.add(ct3);

        return content;
    }

    public static JsonNode getHeaderText() {
        ObjectNode result = Json.newObject();
        result.put("mainTitle", homepageHeaderTextMainTitle);
        result.put("subTitle", homepageHeaderTextSubTitle);
        ArrayNode social = new ArrayNode(JsonNodeFactory.instance);
        ObjectNode google = Json.newObject();
        google.put("url", socialBtnGoogle[0]);
        google.put("text", socialBtnGoogle[1]);
        google.put("icon", socialBtnGoogle[2]);
        social.add(google);
        result.set("social", social);

        return result;
    }

    public static JsonNode getBannerText() {
        ObjectNode result = Json.newObject();
        result.put("message", homepageBannerMessage);
        ArrayNode social = new ArrayNode(JsonNodeFactory.instance);
        ObjectNode google = Json.newObject();
        google.put("url", socialBtnGoogle[0]);
        google.put("text", socialBtnGoogle[1]);
        google.put("icon", socialBtnGoogle[2]);
        social.add(google);
        result.set("social", social);

        return result;
    }

    public static JsonNode getActivityInfo(Activity activity) {
        ObjectNode result = Json.newObject();
        result.put("name", activity.getName());
        result.put("priority", "" + activity.getPriority().id);

        return result;
    }

    public static JsonNode getLabelForms() {
        ObjectNode result = Json.newObject();
        result.put("close", close);
        result.put("submit", submit);
        return result;
    }
}
