package controllers.plot;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Activity;
import models.Priority;
import models.User;
import play.libs.Json;

import java.text.DecimalFormat;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.List;

/**
 * Perfaz operações em gráficos, referente a semana atual.
 */
public class PlotWeek implements Plot {

    public User user;
    public LocalDateTime lastSunday;
    public LocalDateTime nextSunday;

    /**
     * Configura um gráfico para examinar atividades do usuário, na semana atual
     * @param user usuário a configurar gráfico
     */
    public PlotWeek(User user){
        this.user = user;
        setLastSunday();
        setNextSunday();
    }

    /**
     * Inicia valor para o último Domingo
     */
    private void setLastSunday() {
        this.lastSunday = LocalDateTime.now().with(TemporalAdjusters.previous(DayOfWeek.SUNDAY));
    }

    /**
     * Inicia valor para o próximo Domingo
     */
    private void setNextSunday() {
        this.nextSunday =  LocalDateTime.now().with(TemporalAdjusters.previous(DayOfWeek.SUNDAY))
                                              .plusWeeks(1);
    }

    @Override
    public JsonNode getBars() {
        ArrayNode bars = new ArrayNode(JsonNodeFactory.instance);
        double total = this.user.getActivities()
                .stream().mapToDouble(i -> i.timeOnPeriod(this.lastSunday, this.nextSunday)).sum();

        List<Activity> lowAct = this.user.getActivities(Priority.BAIXA),
                       mediumAct = this.user.getActivities(Priority.MEDIA),
                       highAct = this.user.getActivities(Priority.ALTA);
        if(!lowAct.isEmpty())
            lowAct.forEach(a -> bars.add(getBar(total, a, "Prioridade Baixa")));
        if(!lowAct.isEmpty() && !mediumAct.isEmpty())
            bars.add(getBar(0.0, new Activity(""), ""));
        if(!mediumAct.isEmpty())
            mediumAct.forEach(a -> bars.add(getBar(total, a, "Prioridade Média")));
        if((!mediumAct.isEmpty() && !highAct.isEmpty()) || (!lowAct.isEmpty() && !highAct.isEmpty()))
            bars.add(getBar(0.0, new Activity(""), ""));
        if(!highAct.isEmpty())
            highAct.forEach(a -> bars.add(getBar(total, a, "Prioridade Alta")));

        return bars;
    }

    /**
     * Retorna barra do gra´fico corretamente configurada
     * @param total total de horas registradas na semana
     * @param a atividade da barra
     * @param priority prioridade da atividade
     * @return dados da barra a ser gerada
     */
    private ObjectNode getBar(double total, Activity a, String priority) {
        double time = a.timeOnPeriod(this.lastSunday, this.nextSunday);

        ObjectNode barData = Json.newObject();
        barData.put("y", a.getName());
        barData.put("a", "" + time);
        barData.put("priority", priority);

        DecimalFormat f = new DecimalFormat("###.##");
        if(total > 0.0) {
            String perc = f.format((time / total) * 100.0) + "%";
            barData.put("percentage", "" + perc);
        }

        return barData;
    }

    @Override
    public JsonNode getLabels() {
        ArrayNode labels = new ArrayNode(JsonNodeFactory.instance);
        String labels1 = "Horas na Semana Atual";
        labels.add(labels1);
        return labels;
    }

    @Override
    public JsonNode getY() {
        ArrayNode y = new ArrayNode(JsonNodeFactory.instance);
        String ya = "a";
        y.add(ya);
        return y;
    }

    @Override
    public JsonNode getColors() {
        ArrayNode colors = new ArrayNode(JsonNodeFactory.instance);
        String colora = "#0b62a4";
        colors.add(colora);
        return colors;
    }

    @Override
    public JsonNode getPeriod() {
        ArrayNode el = new ArrayNode(JsonNodeFactory.instance);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String start = this.lastSunday.format(formatter);
        String end   = this.nextSunday.format(formatter);
        return el.add("De " + start + " a " + end);
    }

    /**
     * Retorna um ranking de atividades para mostrar na página inicial
     * @return ranking de atividades para mostrar na página inicial
     */
    public JsonNode getRanking(){
        ArrayNode list = new ArrayNode(JsonNodeFactory.instance);
        List<Activity> acts = this.user.getActivities();
        acts.sort((o1, o2) -> (new Double(o2.timeOnPeriod(this.lastSunday, this.nextSunday)))
                               .compareTo(o1.timeOnPeriod(this.lastSunday, this.nextSunday)));
        double total = this.user.getActivities()
                .stream().mapToDouble(i -> i.timeOnPeriod(this.lastSunday, this.nextSunday)).sum();

        for(int i = 0; i < acts.size(); i++){
            double time = acts.get(i).timeOnPeriod(this.lastSunday, this.nextSunday);

            ObjectNode e = Json.newObject();
            e.put("place", i + 1);
            e.put("activ", acts.get(i).getName());
            e.put("total", "" + time);
            if(total > 0.0){
                DecimalFormat f = new DecimalFormat("###.##");
                String perc = f.format((time/total) * 100.0);
                e.put("propo", "" + perc);
            }
            list.add(e);
        }

        return list;
    }
}
