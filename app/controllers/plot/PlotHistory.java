package controllers.plot;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Activity;
import models.Period;
import models.Priority;
import models.User;
import play.libs.Json;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.List;

/**
 * Perfaz operações em gráficos referentes a até duas semanas anteriores.
 */
public class PlotHistory implements Plot {

    public User user;
    public Period a;
    public LocalDateTime aLastSunday;
    public LocalDateTime aNextSunday;
    public Period b;
    public LocalDateTime bLastSunday;
    public LocalDateTime bNextSunday;

    /**
     * Configura um gráfico para examinar atividades do usuário, fazendo um comparativo das semanas
     * @param user usuário a configurar gráfico
     */
    public PlotHistory(User user){
        this.user = user;
        setPeriodA(user.getPreferences().getPeriodSelectedInHistoryA());
        setPeriodB(user.getPreferences().getPeriodSelectedInHistoryB());
    }

    @Override
    public JsonNode getBars() {
        ArrayNode bars = new ArrayNode(JsonNodeFactory.instance);

        List<Activity> lowAct = this.user.getActivities(Priority.BAIXA),
                mediumAct = this.user.getActivities(Priority.MEDIA),
                highAct = this.user.getActivities(Priority.ALTA);
        if(!lowAct.isEmpty())
            lowAct.forEach(a -> bars.add(getBar(a, "Prioridade Baixa")));
        if(!lowAct.isEmpty() && !mediumAct.isEmpty())
            bars.add(getBar(new Activity(""), ""));
        if(!mediumAct.isEmpty())
            mediumAct.forEach(a -> bars.add(getBar(a, "Prioridade Média")));
        if((!mediumAct.isEmpty() && !highAct.isEmpty()) || (!lowAct.isEmpty() && !highAct.isEmpty()))
            bars.add(getBar(new Activity(""), ""));
        if(!highAct.isEmpty())
            highAct.forEach(a -> bars.add(getBar(a, "Prioridade Alta")));

        return bars;
    }

    private JsonNode getBar(Activity a, String priority) {
        double timeA = a.timeOnPeriod(this.aLastSunday, this.aNextSunday);
        double timeB = a.timeOnPeriod(this.bLastSunday, this.bNextSunday);

        ObjectNode barData = Json.newObject();
        barData.put("y", a.getName());
        barData.put("a", "" + timeA);
        barData.put("b", "" + timeB);
        barData.put("priority", priority);
        return barData;
    }

    @Override
    public JsonNode getLabels() {
        ArrayNode labels = new ArrayNode(JsonNodeFactory.instance);
        labels.add(getLabelForPeriod(a));
        labels.add(getLabelForPeriod(b));
        return labels;
    }

    private String getLabelForPeriod(Period p) {
        switch (p){
            case ACTUAL_WEEK:
                return "Horas nesta semana";
            case WEEK_AGO:
                return "Horas na semana passada";
            case TWO_WEEKS_AGO:
                return "Horas há duas semanas";
            default:
                return null;
        }
    }

    @Override
    public JsonNode getY() {
        ArrayNode y = new ArrayNode(JsonNodeFactory.instance);
        String ya = "a";
        y.add(ya);
        String yb = "b";
        y.add(yb);
        return y;
    }

    @Override
    public JsonNode getColors() {
        ArrayNode colors = new ArrayNode(JsonNodeFactory.instance);
        String colora = "#0b62a4";
        colors.add(colora);
        String colorb = "#cb4b4b";
        colors.add(colorb);
        return colors;
    }

    @Override
    public JsonNode getPeriod() {
        ArrayNode periods = new ArrayNode(JsonNodeFactory.instance);

        DateTimeFormatter formatter  = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String start = this.aLastSunday.format(formatter);
        String end   = this.aNextSunday.format(formatter);
        periods.add("De " + start + " a " + end);
        start = this.bLastSunday.format(formatter);
        end   = this.bNextSunday.format(formatter);
        periods.add("De " + start + " a " + end);
        return periods;
    }

    public void setPeriodA(Period a) {
        this.a = a;
        initPeriodA();
        switch (a){
            case WEEK_AGO:
                this.aLastSunday = this.aLastSunday.minusWeeks(1);
                this.aNextSunday = this.aNextSunday.minusWeeks(1);
                break;
            case TWO_WEEKS_AGO:
                this.aLastSunday = this.aLastSunday.minusWeeks(2);
                this.aNextSunday = this.aNextSunday.minusWeeks(2);
                break;
            default:
                break;
        }
    }

    private void initPeriodA() {
        this.aLastSunday = LocalDateTime.now().with(TemporalAdjusters.previous(DayOfWeek.SUNDAY));
        this.aNextSunday = LocalDateTime.now().with(TemporalAdjusters.previous(DayOfWeek.SUNDAY))
                                              .plusWeeks(1);
    }

    public void setPeriodB(Period b) {
        this.b = b;
        initPeriodB();
        switch (b){
            case WEEK_AGO:
                this.bLastSunday = this.bLastSunday.minusWeeks(1);
                this.bNextSunday = this.bNextSunday.minusWeeks(1);
                break;
            case TWO_WEEKS_AGO:
                this.bLastSunday = this.bLastSunday.minusWeeks(2);
                this.bNextSunday = this.bNextSunday.minusWeeks(2);
                break;
            default:
                break;
        }
    }

    private void initPeriodB() {
        this.bLastSunday = LocalDateTime.now().with(TemporalAdjusters.previous(DayOfWeek.SUNDAY));
        this.bNextSunday = LocalDateTime.now().with(TemporalAdjusters.previous(DayOfWeek.SUNDAY))
                .plusWeeks(1);
    }
}
