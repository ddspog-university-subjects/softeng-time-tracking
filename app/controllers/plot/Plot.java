package controllers.plot;

import com.fasterxml.jackson.databind.JsonNode;
/**
 * Interface declarando operações em gráficos.
 */
public interface Plot {

    /**
     * Retorna dados das barras do gráfico em barras
     * @return dados das barras do gráfico em barras
     */
    JsonNode getBars();

    /**
     * Retorna nomes para cada tipo de barra
     * @return nomes para cada tipo de barra
     */
    JsonNode getLabels();

    /**
     * Retorna tipos de barras
     * @return tipos de barras
     */
    JsonNode getY();

    /**
     * Retorna cores para cada barra
     * @return cores para cada barra
     */
    JsonNode getColors();

    /**
     * Retorna texto descrevendo período analisado
     * @return texto descrevendo período analisado
     */
    JsonNode getPeriod();
}

