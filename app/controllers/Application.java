package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.plot.PlotHistory;
import controllers.plot.PlotWeek;
import models.*;
import models.dao.GenericDAOImpl;
import org.apache.commons.lang.NullArgumentException;
import org.pac4j.oauth.profile.google2.Google2Profile;
import org.pac4j.play.ApplicationLogoutController;
import org.pac4j.play.java.RequiresAuthentication;
import org.pac4j.play.java.UserProfileController;
import play.Logger;
import play.data.DynamicForm;
import play.db.jpa.Transactional;
import play.libs.Json;

import play.mvc.Result;

import javax.management.openmbean.KeyAlreadyExistsException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import java.util.List;

public class Application extends UserProfileController<Google2Profile>{

    private static GenericDAOImpl dao = new GenericDAOImpl();

    /**
     * Retorna a página inicial, que pode ser a Dashboard se o usuário já estiver logado.
     * @return página inicial do site
     */
    @Transactional
    public Result index() {
        if(getUserProfile() == null){
            return home();
        } else {
            return dash();
        }
    }

    /**
     * Retorna a página inicial (Homepage)
     * @return página inicial do site
     */
    @Transactional
    private Result home() {
        return ok(views.html.homepage.index.render("Where Goes My Time?"));
    }

    /**
     * Retorna a página inicial (Dashboard)
     * @return página inicial do site
     */
    @Transactional
    private Result dash(){
        getUser();
        return ok(views.html.dashboard.index.render("Where Goes My Time?"));
    }

    /**
     * Autentica o usuário, o registra se preciso, e o redireciona para a página inicial (Dashboard)
     * @return página inicial do site
     */
    @RequiresAuthentication(clientName = "Google2Client")
    public Result googleIndex(){
        return redirect(routes.Application.index());
    }

    /**
     * Retorna o usuário a partir do profile logado, e o cria se necessário
     * @return usuário logado ou recém-registrado
     */
    @Transactional
    private User getUser() {
        Google2Profile profile = getUserProfile();
        String id = profile.getId();
        String name = profile.getEmail();

        Object user = dao.findByEntityId(User.class, id);

        if(user == null){
            return registerUser(id, name);
        } else {
            return (User) user;
        }
    }

    /**
     * Retorna um JSON contendo texto para o imutável da Dashboard
     * @return texto para o imutável da Dashboard
     */
    @Transactional
    public Result getDashboardText() {
        String name = getUser().getName();

        ObjectNode result = Json.newObject();
        result.set("navbarText", Text.getNavbarText(name));
        return ok(result);
    }

    /**
     * Retorna um JSON contendo texto para o imutável da Homepage
     * @return texto para o imutável da Dashboard
     */
    @Transactional
    public Result getHomepageText() {
        ObjectNode result = Json.newObject();
        result.set("navbarText", Text.getNavbarText());
        result.set("footbarText", Text.getFootbarText());
        result.set("headerText", Text.getHeaderText());
        result.set("bannerText", Text.getBannerText());
        result.set("content", Text.getContentHomepage());
        return ok(result);
    }

    /**
     * Retorna um JSON contendo texto para a seção inicial da Dashboard
     * @return texto para a seção inicial da Dashboard
     */
    @Transactional
    public Result getInitialText() {
        User user = getUser();

        ObjectNode result = Json.newObject();
        result.set("lasttis", Text.getLastTisText(user));
        result.set("lastactivities", Text.getLastActivitesText());
        result.set("labels", Text.getLabelInitialPage());
        return ok(result);
    }

    /**
     * Retorna um JSON contendo texto para a seção Semana Atual da Dashboard
     * @return texto para a seção Semana Atual da Dashboard
     */
    @Transactional
    public Result getWeekText() {
        Object user = getUser();
        PlotWeek plot = new PlotWeek((User) user);

        ObjectNode result = Json.newObject();
        result.set("labels", Text.getLabelWeekPage());
        result.set("graph", Text.getGraphData(plot));
        result.set("ranking", Text.getRankingData(plot));
        return ok(result);
    }

    /**
     * Retorna um JSON contendo texto para a seção Histórico da Dashboard
     * @return texto para a seção Histórico da Dashboard
     */
    @Transactional
    public Result getHistoryText() {
        Object user = getUser();
        PlotHistory plot = new PlotHistory((User) user);

        ObjectNode result = Json.newObject();
        result.set("labels", Text.getLabelHistoryPage());
        result.set("graph", Text.getGraphData(plot));
        result.set("preference", Text.getHistoryOptions(user));
        return ok(result);
    }

    /**
     * Retorna um JSON contendo texto para o Form que registra atividades
     * @return texto para o Form que registra atividades
     */
    @Transactional
    public Result getFormActivityText() {
        User user = getUser();

        ObjectNode result = Json.newObject();
        result.set("modalactivity", Text.getModalActivityText(user));
        result.set("modallabels", Text.getLabelForms());
        return ok(result);
    }

    /**
     * Retorna um JSON contendo texto para o Form que registra TI
     * @return texto para o Form que registra atividades
     */
    @Transactional
    public Result getFormTIText() {
        User user = getUser();

        ObjectNode result = Json.newObject();
        result.set("modalti", Text.getModalTiText(user));
        result.set("modallabels", Text.getLabelForms());
        return ok(result);
    }

    @Transactional
    public Result getActivities(String page) {
        User user = getUser();

        ObjectNode result = Json.newObject();
        result.put("totalActivities", user.getActivities().size());
        result.set("activities", Text.getLastActivities(user, Integer.parseInt(page)));
        return ok(result);
    }

    @Transactional
    public Result getActivityInfo(String id) {
        ObjectNode result = Json.newObject();
        Activity activity = dao.findByEntityId(Activity.class, id);
        result.set("activity", Text.getActivityInfo(activity));
        return ok(result);
    }

    /**
     * Registra atividades no sistema
     * @return uma resposta dizendo se o registro foi bem sucedido, ou não e porquê
     */
    @Transactional
    public Result registerActivity() {
        DynamicForm filledForm = new DynamicForm().bindFromRequest();
        User user = getUser();

        if (filledForm.hasErrors()) {
            return badRequest("Erro ao receber os dados.");
        } else {
            String id = filledForm.get("id");
            if(id.equals("")){
                String name = filledForm.get("name");
                Priority priority = Priority.valueOf(Integer.parseInt(filledForm.get("priority")));
                Activity a = new Activity(name, priority);
                try{
                    user.addActivity(a);
                    Logger.info("Registrando " + name + " no sistema...");
                    dao.persist(a);
                    dao.merge(user);
                    dao.flush();

                    Logger.info("Atividade " + name + " registrada!");
                } catch (NullArgumentException e){
                    return badRequest(e.getMessage());
                } catch (KeyAlreadyExistsException e){
                    return badRequest(e.getMessage());
                }
            } else {
                Activity activity = dao.findByEntityId(Activity.class, id);
                String name = filledForm.get("name");
                Priority priority = Priority.valueOf(Integer.parseInt(filledForm.get("priority")));
                activity.setName(name);
                activity.setPriority(priority);
                activity.setLastUpdate(LocalDateTime.now());
                try{
                    Logger.info("Atualisando " + name + " no sistema...");
                    dao.merge(activity);
                    dao.flush();

                    Logger.info("Atividade " + name + " atualizada!");
                }
                catch (Exception e){
                    return badRequest(e.getMessage());
                }
            }

        }

        return ok("Atividade criada com sucesso!");
    }

    /**
     * Registra TIs no sistema
     * @return uma resposta dizendo se o registro foi bem sucedido, ou não e porquê
     */
    @Transactional
    public Result registerTI() {
        DynamicForm filledForm = new DynamicForm().bindFromRequest();

        User user = getUser();

        if (filledForm.hasErrors()) {
            return badRequest("Erro ao receber os dados.");
        } else {
            String name = filledForm.get("nameActivity");
            double time = Double.valueOf(filledForm.get("timeTI"));
            TI ti = new TI(time);
            Activity ac = user.getActivity(name);
            try{
                if(ac == null){
                    ac = new Activity(name);
                    dao.persist(ac);
                    user.addActivity(ac);
                }
                user.addTi(ti, ac);
                dao.persist(ti);
                dao.merge(user);
                dao.flush();

                Logger.info("TI " + name + " de "  + time +  " horas, registrada!");
            }
            catch (Exception e){
                return badRequest(e.getMessage());
            }
        }

        return ok("TI criada com sucesso!");
    }

    /**
     * Registra semanas selecionadas em Histórico
     * @return uma resposta dizendo se o registro foi bem sucedido, ou não e porquê
     */
    @Transactional
    public Result registerHistoryWeeks() {
        DynamicForm filledForm = new DynamicForm().bindFromRequest();

        User user = getUser();

        if (filledForm.hasErrors()) {
            return badRequest("Erro ao receber os dados.");
        } else {
            Logger.info("Trying to register week option on History...");
            Logger.info("weekA = " + filledForm.get("weekA"));
            Logger.info("weekB = " + filledForm.get("weekB"));
            int weekA = Integer.parseInt(filledForm.get("weekA"));
            int weekB = Integer.parseInt(filledForm.get("weekB"));
            try{
                user.getPreferences().setPeriodSelectedInHistoryA(Period.valueOf(weekA));
                user.getPreferences().setPeriodSelectedInHistoryB(Period.valueOf(weekB));
                dao.merge(user);
                dao.flush();

                Logger.info("Opções de semanas em Histórico, registrada!");
            }
            catch (Exception e){
                return badRequest(e.getMessage());
            }
        }

        return ok("Opções de semanas em Histórico registrada com sucesso!");
    }

    /**
     * Registra um novo Uusário no sistema
     * @param id do usuário
     * @param name do usuário
     * @return usuário recém-registrado
     */
    @Transactional
    private User registerUser(String id, String name) {
        User user = new User(name);
        user.setId(id);

        generateSomeData(user);

        dao.persist(user);
        dao.flush();
        return user;
    }

    /**
     * Gera dados padrões para novos usuários
     * @param user usuário a ser registrado
     */
    @Transactional
    private void generateSomeData(User user) {
        List<Activity> activities = new ArrayList<>();
        List<TI> tis = new ArrayList<>();
        activities.add(new Activity("Dormir", Priority.MEDIA));
        activities.add(new Activity("Comer", Priority.MEDIA));
        activities.forEach(a -> dao.persist(a));
        dao.flush();
        tis.add(new TI(8.0, genDate(-8)));
        tis.add(new TI(4.0, genDate(-8)));
        tis.add(new TI(8.0, genDate(-9)));
        tis.add(new TI(4.0, genDate(-9)));
        tis.add(new TI(8.0, genDate(-10)));
        tis.add(new TI(4.0, genDate(-10)));
        tis.forEach(t -> dao.persist(t));
        dao.flush();
        user.addActivity(activities.get(0));
        user.addActivity(activities.get(1));
        user.addTi(tis.get(0), activities.get(0));
        user.addTi(tis.get(1), activities.get(1));
        user.addTi(tis.get(2), activities.get(0));
        user.addTi(tis.get(3), activities.get(1));
        user.addTi(tis.get(4), activities.get(0));
        user.addTi(tis.get(5), activities.get(1));
    }

    /**
     * Gera uma Data com base na data atual
     * @param i dias a somar na data
     * @return data atual + i
     */
    private LocalDateTime genDate(int i) {
        return LocalDateTime.now().plusDays(i);
    }

}