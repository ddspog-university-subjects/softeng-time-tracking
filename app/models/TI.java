package models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Classe que modela o Tempo Investido (TI) do sistema
 * Created by Aline Costa on 08/03/2016.
 */
@Table(name="ti_table")
@Entity(name="TI")
public class TI {

    @Id @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column
    private String id;
    @Column
    private double time;
    @Column
    private LocalDateTime date;
    @OneToOne(cascade=CascadeType.ALL)
    @JoinTable(name="ti_activity",
            joinColumns={@JoinColumn(name="ti_id", referencedColumnName="id")},
            inverseJoinColumns={@JoinColumn(name="activity_id", referencedColumnName="id")})
    private Activity activity;

    /**
     * Construtor JavaBeans.
     */
    public TI(){
    }

    /**
     * Construtor padrão. Cria o novo TI na data atual.
     * @param time o tempo investido (TI)
     */
    public TI(double time) {
        if (time <= 0.0 || time > 24.0) {
            throw new IllegalArgumentException(
                    "O TI deve ter valor válido (0h-24h]");
        }
        this.time = time;
        this.date = LocalDateTime.now();
    }

    /**
     * Construtor para TIs adicionadas em data diferente da atual.
     * @param time o tempo investido (TI)
     * @param date a data do TI
     */
    public TI(double time, LocalDateTime date) {
        if (time <= 0.0 || time > 24.0) {
            throw new IllegalArgumentException(
                    "O TI deve ter valor válido (0h-24h]");
        }
        this.time = time;
        this.date = date;
    }

    /**
     * Recupera o tempo investido (TI)
     * @return o TI
     */
    public double getTime() {
        return time;
    }

    /**
     * Modifica o tempo investido (TI)
     * @param time o novo TI
     */
    public void setTime(double time) {
        if (time <= 0.0 || time > 24.0) {
            throw new IllegalArgumentException(
                    "O TI deve ter valor válido (0h-24h]");
        }
        this.time = time;
    }

    /**
     * Recupera a data do tempo investido (TI)
     * @return a data do TI
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     * Modifica a data do tempo investido (TI)
     * @param date a nova data do TI
     */
    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    /**
     * Recupera a atividade a qual o TI está associado
     * @return a atividade do TI
     */
    public Activity getActivity() {
        return activity;
    }

    /**
     * Recupera id da TI
     * @return id da TI
     */
    public String getId() {
        return id;
    }

    /**
     * Recupera o usuário a qual o TI está associado
     * @return o usuário do TI
     */
    public User getUser() {
        return this.activity.getUser();
    }

    /**
     * Modifica o usuário a qual o TI está associado
     * @param user usuário do TI
     */
    public void setUser(User user) {
        this.activity.setUser(user);
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}
