package models;

public enum Priority {
	BAIXA(-1), MEDIA(0), ALTA(1);

	public int id;

	Priority(final int p){ id = p; }

	public static Priority valueOf(int v) {
		for (Priority my: Priority.values()){
			if (my.id == v){
				return my;
			}
		}
		return null;
	}
}