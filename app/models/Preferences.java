package models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Classe que configura preferências e configurações do Usuário.
 */
@Table(name="preferences_table")
@Entity(name="Preferences")
public class Preferences {

    @Id @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column
    private String id;

    @Column
    private Period periodSelectedInHistoryA;
    @Column
    private Period periodSelectedInHistoryB;

    @OneToOne(cascade= CascadeType.ALL)
    @JoinTable(name="preferences_user",
            joinColumns={@JoinColumn(name="preferences_id", referencedColumnName="id")},
            inverseJoinColumns={@JoinColumn(name="user_id", referencedColumnName="id")})
    private User user;

    public Preferences(){
        this.periodSelectedInHistoryA = Period.ACTUAL_WEEK;
        this.periodSelectedInHistoryB = Period.WEEK_AGO;
    }

    public Period getPeriodSelectedInHistoryA() {
        return periodSelectedInHistoryA;
    }

    public void setPeriodSelectedInHistoryA(Period periodSelectedInHistoryA) {
        this.periodSelectedInHistoryA = periodSelectedInHistoryA;
    }

    public Period getPeriodSelectedInHistoryB() {
        return periodSelectedInHistoryB;
    }

    public void setPeriodSelectedInHistoryB(Period periodSelectedInHistoryB) {
        this.periodSelectedInHistoryB = periodSelectedInHistoryB;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
