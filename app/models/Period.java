package models;

/**
 * Enumeração de períodos abordados em nosso Histórico.
 */
public enum Period{
    ACTUAL_WEEK(0), WEEK_AGO(1), TWO_WEEKS_AGO(2);

    public int id;

    Period(final int p){
        id = p;
    }

    public static Period valueOf(int v) {
        if(v < 0 || v > 2)
            throw new IllegalArgumentException("Value " + v + " is out of scope {0, 1, 2}");
        for (Period my: Period.values()) {
            if (my.id == v) {
                return my;
            }
        }
        return null;
    }
}
