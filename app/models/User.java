package models;

import org.apache.commons.lang.NullArgumentException;

import javax.management.openmbean.KeyAlreadyExistsException;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Classe que modela um usuário do sistema
 * Created by Aline Costa on 07/03/2016.
 */
@Table(name="user_table")
@Entity(name="User")
public class User {

    @Id
    @Column
    private String id;
    @Column
    private String name;
    @OneToMany(cascade=CascadeType.ALL)
    @JoinTable(name="activity_user",
            joinColumns={@JoinColumn(name="user_id", referencedColumnName="id")},
            inverseJoinColumns={@JoinColumn(name="activity_id", referencedColumnName="id")})
    private List<Activity> activities;

    @OneToOne(cascade= CascadeType.ALL)
    @JoinTable(name="preferences_user",
            joinColumns={@JoinColumn(name="user_id", referencedColumnName="id")},
            inverseJoinColumns={@JoinColumn(name="preferences_id", referencedColumnName="id")})
    private Preferences preferences;

    /**
     * Construtor Javabeans
     */
    public User(){
    }

    /**
     * Construtor padrão
     * @param name nome do usuário
     */
    public User(String name) {
        setName(name);
        this.activities = new ArrayList<>();
        this.preferences = new Preferences();
    }

    /**
     * Recupera o nome do usuário
     * @return o nome do usuário
     */
    public String getName() {
        return name;
    }

    /**
     * Modifica o nome do usuário
     * @param name o novo nome do usuário
     */
    public void setName(String name) {
        if(name == null)
            throw new NullArgumentException("O nome setado para o Usuário é nulo");
        this.name = name;
    }

    /**
     * Recupera id do usuário
     * @return id do usuário
     */
    public String getId() {
        return id;
    }

    /**
     * Modifica a id do usuário
     * @param id do usuário
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Adiciona atividade feita pelo usuário
     * @param a atividade do usuário
     */
    public void addActivity(Activity a){
        if(a == null)
            throw new NullArgumentException("A atividade a ser adicionada tem valor nulo.");
        Activity activity = getActivity(a.getName());
        if(activity == null) {
            a.setLastUpdate(LocalDateTime.now());
            this.activities.add(a);
        } else
            throw new KeyAlreadyExistsException("A atividade " + a.getName() + " já existe.");
    }

    /**
     * Recupera uma atividadie feita pelo usuário
     * @param name nome da atividade do usuário
     */
    public Activity getActivity(String name){
        if(name == null)
            throw new NullArgumentException("O nome dado para procurar por uma Atividade é nulo.");
        for (Activity activity : this.activities) {
            if (name.toLowerCase().equals(activity.getName().toLowerCase()))
                return activity;
        }
        return null;
    }

    /**
     * Recupera a lista de atividades feitas pelo usuário
     */
    public List<Activity> getActivities(){
        this.activities.sort((o1, o2) -> (o2.getLastUpdate().compareTo(o1.getLastUpdate())));
        return this.activities;
    }

    /**
     * Recupera a lista de atividades por prioridade
     * @param priority choosen
     */
    public List<Activity> getActivities(Priority priority){
        return getActivities().stream().filter(activity -> activity.getPriority() == priority).collect(Collectors.toList());
    }

    /**
     * Adiciona TI feita pelo usuário
     * @param ti tempo investido pelo usuário
     */
    public void addTi(TI ti, Activity activity)
    {
        double time = ti.getTime();
        LocalDateTime date = ti.getDate();
        activity.addTI(new TI(time, date));
        activity.setLastUpdate(date);
    }

    /**
     * Recupera a lista de tis feitas pelo usuário
     */
    public List<TI> getTis(){
        List<TI> result = this.activities.stream().flatMap(f -> f.getTis().stream()).collect(Collectors.toList());
        result.sort((o1, o2) -> o1.getDate().compareTo(o2.getDate()));
        return result;
    }

    /**
     * Recupera as preferências do usuário
     * @return preferências do usuário
     */
    public Preferences getPreferences() {
        return preferences;
    }

    /**
     * Configura as preferências do usuário
     * @param preferences do usuário
     */
    @SuppressWarnings("unused")
    public void setPreferences(Preferences preferences) {
        this.preferences = preferences;
    }
}
