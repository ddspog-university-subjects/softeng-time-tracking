package models;

import org.apache.commons.lang.NullArgumentException;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;
import java.util.List;
import java.util.ArrayList;
import javax.persistence.*;

/**
 * Classe que modela as atividades do sistema
 * @author Andrew Feliphe 
 */
@Table(name="activity_table")
@Entity
public class Activity {

	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	@Column
    private String id;
    @Column
    private String name;
    @Column
    private Priority priority;

	@OneToOne(cascade= CascadeType.ALL)
	@JoinTable(name="activity_user",
			joinColumns={@JoinColumn(name="activity_id", referencedColumnName="id")},
			inverseJoinColumns={@JoinColumn(name="user_id", referencedColumnName="id")})
	private User user;

    @OneToMany(cascade=CascadeType.ALL)
    @JoinTable(name="ti_activity",
            joinColumns={@JoinColumn(name="activity_id", referencedColumnName="id")},
            inverseJoinColumns={@JoinColumn(name="ti_id", referencedColumnName="id")})
    private List<TI> tis;
	@Column
	private LocalDateTime lastUpdate;

	/**
     * Construtor JavaBeans
     */
	public Activity(){
    }

	/**
	 * Metodo construtor para Activity com prioridade default
	 * @param name O nome da Activity
	 */
	public Activity(String name) {
		setName(name);
		this.tis = new ArrayList<>();
		this.priority = Priority.MEDIA;
	}
	
	/**
	 * Metodo construtor para Activity com prioridade definida
	 * @param name O nome da atividade
	 * @param priority A prioridade da Activity
	 */
	public Activity(String name, Priority priority){
		setName(name);
		this.tis = new ArrayList<>();
		this.priority = priority;
	}

	/**
	 * Metodo para recuperar a prioridade desta atividade
	 * @return A prioridade desta atividade
	 */
	public Priority getPriority() {
		return priority;
	}

	/**
	 * Metodo para redefinir a prioridade da atividade
	 * @param priority A nova prioridade da atividade
	 */
	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	/**
	 * Metodo para adicionar um novo Tempo investido (TI) nesta atividade
	 * @param timeInvested TI na atividade
	 */
	public void addTI(TI timeInvested){
		if (timeInvested == null) {
			throw new IllegalArgumentException(
					"O TI não pode ser nulo");
		}
        this.tis.add(timeInvested);
	}

	/**
	 * Metodo para recuperar o nome da atividade
	 * @return nome da atividade
	 */
	public String getName() {
		return name;
	}

	/**
	 * Metodo para atualizar o nome da atividade
	 * @param name O novo nome desta atividade
	 */
	public void setName(String name) {
        if(name == null)
            throw new NullArgumentException("O nome setado para a atividade é nulo");
		this.name = name;
	}

	/**
	 * Metodo para recuperar a lista de todas as TIs nesta atividade
	 * @return A lista de todas as TIs nesta atividade
	 */
	public List<TI> getTis() {
		return this.tis;
	}

	/**
	 * Recupera o tempo total investido na Activity, no período dado
	 * @param start data de início do período pesquisado
	 * @param finish data final do período pesquisado
	 * @return o tempo total investido na atividade, no período dado
     */
	public double timeOnPeriod(LocalDateTime start, LocalDateTime finish) {
		double total = 0;
		for (TI e: this.tis) {
			if(start.isBefore(e.getDate()) && finish.isAfter(e.getDate()))
				total += e.getTime();
		}
		return total;
	}

    /**
     * Recupera id da atividade
     * @return id da atividade
     */
    public String getId() {
        return id;
    }

    /**
     * Recupera o usuário da atividade
     * @return usuário da atividade
     */
    public User getUser() {
        return user;
    }

    /**
     * Metodo para atualizar o usuário da atividade
     * @param user usuário da atividade
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Recupera data da última atualização na atividade
     * @return data da última atualização na atividade
     */
    public LocalDateTime getLastUpdate(){
        return this.lastUpdate;
    }

    /**
     * Metodo para atualizar a data da última atualização
     * @param lastUpdate data da última atualização da atividade
     */
	public void setLastUpdate(LocalDateTime lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
}
