var dashApp = angular.module('DashboardApp', ['ui.router', 'angularUtils.directives.dirPagination', 'angucomplete-alt'])
    .config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider){

        // For any unmatched url, redirect to /initial
        $urlRouterProvider.otherwise("/");

        // States
        $stateProvider
            .state('initial', {
                url: "",
                templateUrl: "/assets/javascripts/dashboard/sections/initial.html"
            })
            .state('initial.registerTI',{
                url: "",
                templateUrl: "/assets/javascripts/dashboard/sections/initial-registerTI.html"
            })
            .state('initial.registerActivity',{
                url: "",
                params: {
                    id: undefined
                },
                templateUrl: "/assets/javascripts/dashboard/sections/initial-registerActivity.html",
                controller: function($scope, $stateParams){
                    $scope.activityId = $stateParams.id;
                }
            })
            .state('week', {
                url: "",
                templateUrl: "/assets/javascripts/dashboard/sections/week.html"
            })
            .state('history', {
                url: "",
                templateUrl: "/assets/javascripts/dashboard/sections/history.html"
            })

    }]);