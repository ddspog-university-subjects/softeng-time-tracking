dashApp.directive('navBar', function(){
    return {
        restrict: 'E',
        scope: {
            item: '=item'
        },
        controller: ['$scope', '$parse', "$http", "$window", function($scope, $parse, $http, $window){
            $scope.callFunc = function(exp){
                $parse(exp)($scope); //Parse the function name to get the expression and invoke it on the scope
            };
            $scope.logout = function () {
                $http.get('/logout').success(function(){
                    $window.location.reload();
                }).error(function(){});
            };
        }],
        templateUrl: '/assets/javascripts/dashboard/directives/navbar.html'
    };
});