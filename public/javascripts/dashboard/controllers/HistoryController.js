dashApp.controller('HistoryController', function($scope, $http) {
    $scope.loadHistorySection = function() {
        $http.get('/api/historytext')
        .then(function successCallback(response){
            $scope.labels = response.data.labels;
            $scope.graph = response.data.graph;
            $scope.preference = response.data.preference;
            //noinspection JSUnresolvedVariable
            $("#panel-bar-chart-graph").empty();
            $("#legend-label").empty();
            $("#legend-period").empty();
            var chart = Morris.Bar({
                element: 'panel-bar-chart-graph',
                data: $scope.graph.barsData,
                xkey: 'y',
                ykeys: $scope.graph.barsY,
                labels: $scope.graph.barsLabels,
                barColors: $scope.graph.barsColors,
                hideHover: 'auto',
                xLabelAngle: 15,
                stacked: false,
                hoverCallback: function(index, options, content){
                    var data = options.data[index];
                    if (data.y == "")
                        return "";
                    else
                        return content;
                },
                resize: true,
                newAxisBottom: 45.0,
                newAxisLabel: 'priority'
            });

            var legendA = $('<div class="col-sm-5 col-sm-offset-2"><span>' +
                chart.options.labels[0] + '</span></div>').css('color', chart.options.barColors[0]);
            var legendB = $('<div class="col-sm-5"><span>' +
                chart.options.labels[1] + '</span></div>').css('color', chart.options.barColors[1]);
            $("#legend-label").append(legendA);
            $("#legend-label").append(legendB);

            var periodA = $('<div class="col-sm-5 col-sm-offset-2"><span>' +
                $scope.graph.period[0] + '</span></div>').css('color', chart.options.barColors[0]);
            var periodB = $('<div class="col-sm-5"><span>' +
                $scope.graph.period[1] + '</span></div>').css('color', chart.options.barColors[1]);
            $('#legend-period').append(periodA);
            $('#legend-period').append(periodB);

            $scope.formData = $scope.preference;

        },    function errorCallback(){
            $scope.labels = {};
            $scope.graph = {};
            $scope.preference = {};
        });
    };
    $scope.loadHistorySection();
});