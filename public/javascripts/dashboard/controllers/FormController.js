dashApp.controller('FormController', function($scope, $http, $state) {

    $scope.init = function(type){
        $scope.formData = {};

        if(type == 'activity'){
            $http.get('/api/formactivitytext')
                .then(function successCallback(response){
                    $scope.modalactivity = response.data.modalactivity;
                    $scope.modallabels = response.data.modallabels;
                },    function errorCallback(){
                    $scope.modalactivity = {};
                    $scope.modallabels = {};
                });
            if($scope.activityId != undefined) {
                $http.get('/api/activityinfo?id=' + $scope.activityId)
                    .then(function successCallback(response) {
                        $scope.activity = response.data.activity;
                        $scope.formData.priority = response.data.activity.priority;
                    }, function errorCallback() {
                        $scope.activity = {};
                    })
            } else {
                $scope.formData.priority = '0';
            }
        }else if(type == 'ti'){
            $http.get('/api/formtitext')
                .then(function successCallback(response){
                    $scope.modalti = response.data.modalti;
                    $scope.modallabels = response.data.modallabels;
                },    function errorCallback(){
                    $scope.modalti = {};
                    $scope.modallabels = {};
                });
        }
    };

    $scope.goTo = function(destination) {
        //noinspection JSUnresolvedFunction
        $state.transitionTo(destination, {}, {
            reload: true,
            inherit: true,
            notify: true
        });
    };

    // process the form
    $scope.processForm = function(type) {
        if($scope.activityId != undefined){
            $scope.formData.id = $scope.activityId;
        } else {
            $scope.formData.id = '';
        }
        $http({
            method  : 'POST',
            url     : '/reg/' + type,
            data    : $scope.formData,  // pass in data as strings
            headers: {'Content-Type': 'application/json'}
        })
            .success(function(data) {
                console.log(data);

                $scope.errorName = null;

                if(type == 'activity' || type == 'ti'){
                    $('#registerModal').modal('hide');
                    $('body').toggleClass('modal-open');
                    $('.modal-backdrop.fade.in').remove();
                    $scope.goTo('initial');
                } else if(type == 'historyweeks'){
                    $scope.loadHistorySection();
                }
            })
            .error(function(data){
                $scope.errorName = data;
        });
    };
});
