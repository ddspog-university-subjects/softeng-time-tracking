dashApp.controller('DashController', function($scope, $http) {
    $http.get('/api/dstext')
        .then(function successCallback(response){
            $scope.navbarText = response.data.navbarText;
        },    function errorCallback(){
            $scope.navbarText = {};
        });
});