dashApp.controller('InitialController', function($scope, $http, $state) {
    $scope.activities = [];
    $scope.totalActivities = 0;
    $scope.activitiesPerPage = 8;
    getResultsPage(1);

    $scope.pagination = {
        current: 1
    };

    $scope.pageChanged = function (newPage) {
        getResultsPage(newPage);
    };

    function getResultsPage(pageNumber){
        $http.get('/api/lastactivities?page=' + pageNumber)
            .then(function successCallback(response){
                $scope.activities = response.data.activities;
                $scope.totalActivities = response.data.totalActivities;
            }, function errorCallback(){
                $scope.activities = [];
                $scope.totalActivities = 0;
            });
    }

    $http.get('/api/initext')
        .then(function successCallback(response){
            $scope.lasttis = response.data.lasttis;
            $scope.lastactivities = response.data.lastactivities;
            $scope.labels = response.data.labels;
            $scope.setPage = function(pg){
                $state.go(pg);
            };
        },    function errorCallback(response){
            $scope.lasttis = {};
            $scope.lastactivities = {};
            $scope.labels = {};
            $scope.setPage = {};
        });


});