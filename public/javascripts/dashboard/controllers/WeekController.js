dashApp.controller('WeekController', function($scope, $http) {
    $http.get('/api/weektext')
        .then(function successCallback(response){
            $scope.labels = response.data.labels;
            $scope.graph = response.data.graph;
            $scope.ranking = response.data.ranking;
            //noinspection JSUnresolvedVariable
            var chart = Morris.Bar({
                element: 'panel-bar-chart-graph',
                data: $scope.graph.barsData,
                xkey: 'y',
                ykeys: $scope.graph.barsY,
                labels: $scope.graph.barsLabels,
                barColors: $scope.graph.barsColors,
                hideHover: 'auto',
                barRatio: 0.4,
                xLabelAngle: 15,
                hoverCallback: function(index, options, content){
                    var data = options.data[index];
                    if (data.y == "")
                        return "";
                    else if(data.percentage != undefined)
                        return $(content).add('<div class="morris-hover-point" style="color: #0b62a4">'
                            + data.percentage +'</div>');
                    else
                        return content;
                },
                resize: true,
                newAxisBottom: 45.0,
                newAxisLabel: 'priority'
            });
        },    function errorCallback(){
            $scope.labels = {};
            $scope.graph = {};
            $scope.ranking = {};
        });
});