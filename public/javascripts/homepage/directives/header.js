homeApp.directive('header', function(){
    return {
        restrict: 'E',
        scope: {
            item: '=item'
        },
        templateUrl: '/assets/javascripts/homepage/directives/header.html'
    };
});