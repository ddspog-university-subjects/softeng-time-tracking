homeApp.directive('banner', function(){
    return {
        restrict: 'E',
        scope: {
            item: '=item'
        },
        templateUrl: '/assets/javascripts/homepage/directives/banner.html'
    };
});