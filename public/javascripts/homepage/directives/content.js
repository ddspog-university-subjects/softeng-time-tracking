homeApp.directive('contentA', function(){
    return {
        restrict: 'E',
        scope: {
            item: '=item'
        },
        templateUrl: '/assets/javascripts/homepage/directives/contenta.html'
    };
});

homeApp.directive('contentB', function(){
    return {
        restrict: 'E',
        scope: {
            item: '=item'
        },
        templateUrl: '/assets/javascripts/homepage/directives/contentb.html'
    };
});