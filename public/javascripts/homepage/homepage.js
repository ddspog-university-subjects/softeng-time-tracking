var homeApp = angular.module('HomepageApp', ['ui.router', 'ngSanitize'])
    .config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider){

        // For any unmatched url, redirect to /initial
        $urlRouterProvider.otherwise("/initial");

        // States
        $stateProvider
            .state('initial', {
                url: "/initial",
                templateUrl: "/assets/javascripts/homepage/sections/initial.html"
            })

    }]);