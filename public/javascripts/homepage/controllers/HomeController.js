homeApp.controller('HomeController', function($scope, $http) {
    $http.get('/api/hptext')
        .then(function successCallback(response){
            $scope.navbarText = response.data.navbarText;
            $scope.footbarText = response.data.footbarText;
            $scope.headerText = response.data.headerText;
            $scope.bannerText = response.data.bannerText;
            $scope.content = response.data.content;
        },    function errorCallback(){
            $scope.navbarText = {};
            $scope.footbarText = {};
            $scope.headerText = {};
            $scope.bannerText = {};
            $scope.content = {};
        });
});