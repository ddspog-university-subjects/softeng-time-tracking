package dao;

import static fake.FActivity.FakeActivity;
import static fake.FUser.FakeUser;
import static org.fest.assertions.Assertions.assertThat;

import models.Activity;
import models.User;
import models.dao.GenericDAO;
import models.dao.GenericDAOImpl;

import org.junit.Test;

/**
 * Testes para analisar inserção de Usuários em Banco de Dados.
 */
public class UserDAOTest extends AbstractTest {
    private GenericDAO dao = new GenericDAOImpl();

    /**
     * Story: Registro de Usuários no banco de dados deve ser possível.
     *
     * Como o sistema usando banco de dados com Usuários
     * Eu quero que toda vez que registro  um Usuário no banco de dados, nenhum erro ocorra
     * Assim posso garantir que tenham sido adicionados corretamente.
     *
     * Dado que um Usuário exista.
     * Quando eu coloco um Usuário no banco de dados
     * Então devo confirmar a operação olhando no banco de dados.
     */
    @Test
    public void shouldRegistrerUser() {
        for(int i = 0; i < 10; i++)
            testRegistrerUser(i);
    }

    /**
     * Testa se é possível persistir um usuário.
     */
    private void testRegistrerUser(int index) {
        User user = FakeUser(index).getNew();

        dao.persist(user);

        User userNoBD = (User) dao.findByAttributeName("User", "key", "" + FakeUser(index).getKey());

        assertThat(userNoBD.getName()).isEqualTo(FakeUser(index).getName());
        assertThat(userNoBD.getKey()).isEqualTo(FakeUser(index).getKey());
    }

    /**
     * Story: Registro de Atividades em Usuários no banco de dados deve ser possível.
     *
     * Como o sistema usando banco de dados com Atividades
     * Eu quero que toda vez que registro  uma Atividade no banco de dados, nenhum erro ocorra
     * Assim posso garantir que tenham sido adicionados corretamente.
     *
     * Dado que um Usuário e uma Atividade exista.
     * Quando eu coloco um Usuário e dentro dele uma Atividade no banco de dados
     * Então devo confirmar a operação olhando no banco de dados.
     */
    @Test
    public void shouldAddActivity() {
        for(int i = 0; i < 10; i++)
            testAddActivity(i);
    }

    /**
     * Testa se é possível dar merge em mudanças em uma dica.
     */
    private void testAddActivity(int index) {
        User user = FakeUser(index).getNew();
        Activity activity = FakeActivity(index).getNew();

        user.addActivity(activity);

        dao.persist(user);

        User userNoBD = (User) dao.findByAttributeName("User", "key", ""+ FakeUser(index).getKey());

        assertThat(userNoBD.getActivities().size() == 1);

        userNoBD.addActivity(FakeActivity(index).getNew());
        dao.merge(userNoBD);

        userNoBD = (User) dao.findByAttributeName("User", "key", ""+ FakeUser(index).getKey());
        assertThat(userNoBD.getActivities().size() == 2);
    }
}