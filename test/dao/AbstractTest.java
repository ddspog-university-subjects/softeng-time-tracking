package dao;

import javax.persistence.EntityManager;

import org.junit.After;
import org.junit.Before;

import play.db.jpa.JPA;
import play.db.jpa.JPAPlugin;
import play.test.FakeApplication;
import play.test.Helpers;
import scala.Option;

/**
 * Esta classe configura uma maneira de fazer testes, que rodem sem interferir no Banco de Dados.
 *
 * Com isto, testes que realizam adições no Banco de Dados, não interferem no Banco de Dados real e utilizado pelo
 * sistema. O desenvolvedor fica a salvo de sua própria desatenção ao criar os testes.
 *
 * Mais informação sobre esse tipo de teste pode ser encontrada em
 *      https://www.playframework.com/documentation/2.4.x/JavaFunctionalTest
 */
public abstract class AbstractTest {
    public EntityManager em;

    @Before
    public void setUp() {
        FakeApplication app = Helpers.fakeApplication();
        Helpers.start(app);
        Option<JPAPlugin> jpaPlugin = app.getWrappedApplication().plugin(JPAPlugin.class);
        em = jpaPlugin.get().em("default");
        JPA.bindForCurrentThread(em);
        em.getTransaction().begin();
    }

    @After
    public void tearDown() {
        em.getTransaction().commit();
        JPA.bindForCurrentThread(null);
        em.close();
    }
}