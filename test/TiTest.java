import static org.junit.Assert.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import models.Activity;
import models.TI;
import java.util.Calendar;
import java.util.GregorianCalendar;


public class TiTest {

	Calendar d, d2;
	TI tiTest, tiTestTime;
	double time = 2.0;

	@Before
	public void setUp() {
		d = GregorianCalendar.getInstance();
		setDate2();

		tiTestTime = new TI(time);
		tiTest = new TI(time, d2);
	}

	/**
	 * Seta o valor de d2 para o último sábado
	 * @return
	 */
	private void setDate2() {
		this.d2 = GregorianCalendar.getInstance();
		this.d2.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		this.d2.set(Calendar.HOUR_OF_DAY, 0);
		this.d2.set(Calendar.MINUTE, 0);
		this.d2.set(Calendar.SECOND, 0);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testTIWithInvalidArgumentTime() {
		TI invalidTI = new TI(-3);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testTIWithInvalidArgumentTime2() {
		TI invalidTI = new TI(25);
	}

	@Test
	public void testGetSetTimeWithValidArgument() {
		Assert.assertTrue("Time should be the same as when it was instatiated", tiTestTime.getTime() == 2.0);
		tiTestTime.setTime(7);
		Assert.assertTrue("Time should be redefined by the set method", tiTestTime.getTime() == 7);
		tiTestTime.setTime(24);
		Assert.assertTrue("Time should be redefined by the set method", tiTestTime.getTime() == 24.0);
	}

	@Test
	public void testGetSetTimeWithInvalidArgument() {
		tiTestTime.setTime(5.0);
		try {
			tiTestTime.setTime(-10);
			Assert.fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			Assert.assertEquals("O TI deve ter valor válido (0h-24h]", e.getMessage());
		}
		Assert.assertTrue("Time should be redefined by the set method", tiTestTime.getTime() == 5.0);

		try {
			tiTestTime.setTime(24.1);
			Assert.fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			Assert.assertEquals("O TI deve ter valor válido (0h-24h]", e.getMessage());
		}
		Assert.assertTrue("Time should be redefined by the set method", tiTestTime.getTime() == 5.0);
	}

	@Test
	public void testGetSetDateWithValidArgument() {
		// ti criado com a data atual
		Assert.assertTrue("Date should be the same as when it was instatiated", tiTestTime.getDate().equals(d));

		// ti criado com data específica
		Assert.assertTrue("Date should be the same as when it was instatiated", tiTest.getDate().equals(d2));
		Assert.assertFalse("Date should be the different of d", tiTest.getDate().equals(d));

		tiTest.setDate(d);
		Assert.assertTrue("Date should be changed", tiTest.getDate().equals(d));
		Assert.assertFalse("Date should be the different of d2", tiTest.getDate().equals(d2));
	}

}
