
import static org.junit.Assert.*;

import models.Priority;
import models.TI;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import models.Activity;
import models.User;

import java.util.ArrayList;
import java.util.List;

public class UserTest {

	User userTest;
	Activity supportActivity, supportActivity2;
	final int FIRST_ELEMENT = 0;

	@Before
	public void setUp() {
		userTest = new User("Test", 1);
		supportActivity = new Activity("Support for User testing");
		supportActivity2 = new Activity("User testing 2", Priority.ALTA);
	}

	@Test
	public void testConstructorWithInvalidValue() {
		try {
			User userConstructorTest1 = new User("Constructor Test", -1);
			Assert.fail("The key value in the constructor method cannot have a negative value or null");
		} catch (IllegalArgumentException e) {
			Assert.assertEquals("O valor key do Usuário deve ter valor maior que zero", e.getMessage());
		}

		try {
			User userConstructorTest1 = new User("Constructor Test", 0);
			Assert.fail("The key value in the constructor method cannot have a negative value or null");
		} catch (IllegalArgumentException e) {
			Assert.assertEquals("O valor key do Usuário deve ter valor maior que zero", e.getMessage());
		}
	}

	@Test
	public void testGetSetName() {
		Assert.assertTrue("Should have the same value of when it was instantiated", userTest.getName() == "Test");
		userTest.setName("New Test");
		Assert.assertTrue("Should have updated the attribute name", userTest.getName() == "New Test");
	}

	@Test
	public void testGetSetKey() {
		Assert.assertTrue("Should have the same value of when it was instantiated", userTest.getKey() == 1);
		userTest.setKey(5);
		Assert.assertTrue("Should have updated the attribute name", userTest.getKey() == 5);

		try {
			userTest.setKey(-10);
			Assert.fail("The key value in the set method cannot have a negative value or null");
		} catch (IllegalArgumentException e) {
			Assert.assertEquals("O valor key do Usuário deve ter valor maior que zero", e.getMessage());
		}
		Assert.assertTrue("Should have updated the attribute name", userTest.getKey() == 5);

		try {
			userTest.setKey(0);
			Assert.fail("The key value in the set method cannot have a negative value or null");
		} catch (IllegalArgumentException e) {
			Assert.assertEquals("O valor key do Usuário deve ter valor maior que zero", e.getMessage());
		}
		Assert.assertTrue("Should have updated the attribute name", userTest.getKey() == 5);
	}

	@Test
	public void testAddActivity() {
		List<Activity> activitiesTest = new ArrayList<Activity>();
		Assert.assertTrue(userTest.getActivities().equals(activitiesTest));
		userTest.addActivity(supportActivity);

		Assert.assertTrue("The activity should be the same that has been added", userTest.getActivities().get(FIRST_ELEMENT).equals(supportActivity));
		Assert.assertTrue("The activity should has been added", userTest.getActivities().contains(supportActivity));

		activitiesTest.add(supportActivity);
		Assert.assertTrue(userTest.getActivities().equals(activitiesTest));

	}

	@Test
	public void testAddActivityAlreadyAdd() {
		userTest.addActivity(supportActivity);
		try {
			userTest.addActivity(supportActivity);
			Assert.fail("The activity should not be add");
		} catch (IllegalArgumentException e) {
			Assert.assertEquals("A atividade Support for User testing jÃ¡ existe.", e.getMessage());
		}
	}

	@Test
	public void testGetActivity() {
		userTest.addActivity(supportActivity2);
		Assert.assertTrue(userTest.getActivity("User testing 2").equals(supportActivity2));

		Assert.assertTrue(userTest.getActivity("User testing") == null);
	}

	@Test
	public void testAddTI() {
		TI ti = new TI(5);

		Assert.assertFalse(supportActivity.getTis().contains(ti));
		Assert.assertTrue(supportActivity.getTis().isEmpty());
		userTest.addTi(ti, supportActivity);

		Assert.assertTrue("This activity should have now one Ti registered", supportActivity.getTis().size() == 1);
	}
}
