package fake;


import models.User;

/**
 * Esta classe, armazena constantes, configurando um design mais belo para criar classes Usuários
 * em testes, livres para serem alteradas. No geral esta classe usa o padrão Singleton, aonde
 * getInstance == FakeUser(index: int).
 *
 * Quando você chamar FakeUser(index), esta classe vai a instância da classe, e retorna apenas a
 * informação do exemplo de Usuário de índice index.
 *
 * Este FUser tem apenas atributo names e keys, porque são atributos exclusivos dele. FActivity e
 * FTI terão seus próprios exemplos de atributos únicos a eles.
 */
public class FUser {
    /** Número de exemplos que temos até agora. **/
    public final static int MAX = 10;
    /** Exemplos de nomes. **/
    private String [] names = {"Jorge Duro", "Cândido Malandro", "Jô Soares", "Kibam Daram", "Letícia Ramos",
                               "Terciel Gomes", "Ana Jabour", "Risu Livelle", "Aram Cardoso", "Francisco Gusmão"};
    /** Exemplos de chaves -- Isso possivelmente irá mudar para String. **/
    private int [] keys = {1222312311, 1313213124, 290371298, 730921721, 312312345, 217938897, 873192370,
                           987349778, 309387129, 1938201983};
    /** Index selecionado em FakeUser(). **/
    private int index;

    /**
     * Retorna acesso a um exemplo de Usuário
     * @param i index selecionado do exemplo requerido.
     * @return acesso a um exemplo de Usuário no index i.
     */
    public static FUser FakeUser(int i){
        FUser fake = SingletonHolder.INSTANCE;

        if(-1 < i && i < MAX){
            fake.index = i;
        }

        return fake;
    }

    /**
     * Retorna uma nova instância do exemplo de Usuário.
     * @return nova instância do exemplo de Usuário.
     */
    public User getNew(){
        return new User(this.names[index], this.keys[index]);
    }

    /**
     * Retorna o nome do exemplo de Usuário.
     * @return o nome do exemplo de Usuário.
     */
    public String getName(){
        return this.names[this.index];
    }

    /**
     * Retorna a chave do exemplo de Usuário.
     * @return a chave do exemplo de Usuário.
     */
    public int getKey(){
        return this.keys[this.index];
    }

    /**
     * Instância necessária para o padrão Singleton.
     */
    private static class SingletonHolder {
        private static final FUser INSTANCE = new FUser();
    }
}
