package fake;

import models.Activity;
import models.Priority;

/**
 * Esta classe, armazena constantes, configurando um design mais belo para criar classes Atividades
 * em testes, livres para serem alteradas. No geral esta classe usa o padrão Singleton, aonde
 * getInstance == FakeActivity(index: int).
 *
 * Quando você chamar FakeActivity(index), esta classe vai a instância da classe, e retorna apenas a
 * informação do exemplo de Atividade de índice index.
 *
 * Este FActivity tem apenas atributo names e prioridades, porque são atributos exclusivos dele. FUser e
 * FTI terão seus próprios exemplos de atributos únicos a eles.
 */
public class FActivity {
    /** Número de exemplos que temos até agora. **/
    public final static int MAX = 2;
    /** Exemplos de nomes. **/
    private String [] names = {"Comer", "Beber", "Dormir", "Trabalho no Escritório", "Prática de Dança",
                               "Estudar Desenho", "Acompanhar Jornais", "Jogar Videogame", "Ver TV", "Sair com amigos"};
    /** Prioridades relacionada a cada exemplo **/
    private Priority [] priorities = {Priority.MEDIA, Priority.MEDIA, Priority.MEDIA, Priority.ALTA, Priority.ALTA,
                                      Priority.MEDIA, Priority.BAIXA, Priority.BAIXA, Priority.BAIXA, Priority.BAIXA};
    /** Index selecionado em FakeActivity(). **/
    private int index;

    /**
     * Retorna acesso a um exemplo de Atividade
     * @param i index selecionado do exemplo requerido.
     * @return acesso a um exemplo de Atividade no index i.
     */
    public static FActivity FakeActivity(int i){
        FActivity fake = SingletonHolder.INSTANCE;

        if(-1 < i && i < MAX){
            fake.index = i;
        }

        return fake;
    }

    /**
     * Retorna uma nova instância do exemplo da Atividade.
     * @return nova instância do exemplo da Atividade.
     */
    public Activity getNew(){
        return new Activity(this.names[index], this.priorities[index]);
    }

    /**
     * Retorna o nome do exemplo da Atividade.
     * @return o nome do exemplo da Atividade.
     */
    public String getName(){
        return this.names[this.index];
    }

    /**
     * Retorna a prioridade do exemplo da Atividade.
     * @return a prioridade do exemplo da Atividade.
     */
    public Priority getPriority(){
        return this.priorities[this.index];
    }

    /**
     * Instância necessária para o padrão Singleton.
     */
    private static class SingletonHolder {
        private static final FActivity INSTANCE = new FActivity();
    }
}
