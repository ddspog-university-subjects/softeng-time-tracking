import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import models.Activity;
import models.Priority;
import models.TI;

public class ActivityTest {

	Activity activityTest, activityTestWithPriority;
	TI ti, tiInvalid;

	@Before
	public void setUp() {
		activityTest = new Activity("Test");
		activityTestWithPriority = new Activity("Test 2", Priority.ALTA);
		ti = new TI(2);
	}

	@Test
	public void testGetSetPriority() {
		Assert.assertTrue("This activity's priority should have been the default value", activityTest.getPriority() == Priority.MEDIA);
		activityTest.setPriority(Priority.BAIXA);
		Assert.assertTrue("This activity's priority should have been reset", activityTest.getPriority() == Priority.BAIXA);

		Assert.assertTrue("This activity's priority should have been the constructor's value", activityTestWithPriority.getPriority() == Priority.ALTA);
		activityTestWithPriority.setPriority(Priority.MEDIA);
		Assert.assertTrue("This activity's priority should have been reset", activityTestWithPriority.getPriority() == Priority.MEDIA);
	}



	@Test
	public void testGetSetName() {
		Assert.assertTrue("This activity's name should have been the same when this object was intstantiated", activityTest.getName() == "Test");
		activityTest.setName("Changed");
		Assert.assertTrue("This activity's name should have been changed", activityTest.getName() == "Changed");

		Assert.assertTrue("This activity's name should have been the same when this object was intstantiated", activityTestWithPriority.getName() == "Test 2");
		activityTestWithPriority.setName("Changed 2");
		Assert.assertTrue("This activity's name should have been changed", activityTestWithPriority.getName() == "Changed 2");
	}

	@Test
	public void testAddTiAndGetTis() {
		Assert.assertTrue("This activity should have no Ti by now", activityTest.getTis().isEmpty());
		activityTest.addTI(ti);
		Assert.assertTrue("This activity should have now one Ti registered", activityTest.getTis().size() == 1);
		Assert.assertTrue("This activity should have now the TI ti registered", activityTest.getTis().contains(ti));

		try {
			tiInvalid = new TI(-2);
			Assert.fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			Assert.assertEquals("O TI deve ter valor válido (0h-24h]", e.getMessage());
		}

		try {
			activityTest.addTI(tiInvalid);
			Assert.fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			Assert.assertEquals("O TI não pode ser nulo", e.getMessage());
		}

		Assert.assertTrue("This activity should have now one Ti registered", activityTest.getTis().size() == 1);
		Assert.assertTrue("This activity should have now the TI ti registered", activityTest.getTis().contains(ti));
		Assert.assertFalse("This activity not should have now the TI tiInvalid registered", activityTest.getTis().contains(tiInvalid));
	}

}
